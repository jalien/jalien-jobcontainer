# JAliEn jobcontainer

Default Singularity container used by JAliEn to execute payloads

Build instructions:
```console
/cvmfs/alice.cern.ch/containers/bin/apptainer/current/bin/apptainer build --fakeroot --sandbox newimagename/ this.def
```
